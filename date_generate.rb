# encoding: utf-8
require 'csv' 
require 'rubygems'

#日付
class Time
    def self.random(years_back=5)
        year = Time.now.year - rand(years_back) - 1
        month = rand(12) + 1
        day = rand(31) + 1
        Time.local(year, month, day)
    end
end




#訪問回数
visiter_count = rand(100)

#流入元サイト
ryuunyuumoto = ["/service/feature.html","","/qanda/index.html"]
#流入キーワード
keyword = ["Rtoaster","","アールトースター"]

CSV.open("./sample_date.csv", "w", :encoding => "UTF-8") do |writer|
	  writer << ["", "訪問回数","ページビュー","訪問間隔","訪問日時","最終アクセス日時","流入元種別","流入元サイト","流入元ページ","流入キーワード"]
	  10.times do
            writer << ["","#{rand(15)}","#{rand(150)}","#{rand(100000000)}","#{Time.random(15)}","#{Time.random(20)}","#{ryuunyuumoto[rand(ryuunyuumoto.length)]}","#{ryuunyuumoto[rand(ryuunyuumoto.length)]}","#{ryuunyuumoto[rand(ryuunyuumoto.length)]}","#{keyword[rand(keyword.length)]}"]
          end

end
